const Discord = require('discord.js');
const bot = new Discord.Client();

const config = require('./config.json');
const fs = require('fs');
const Table = require('cli-table2');

var games = new Map();

function writeInFile(data, filename){
  fs.writeFile(filename, JSON.stringify(data), function(err) {
      if(err) {
          return console.log(err);
      }
      console.log(`[SUCCESS] File ${filename} has been updated`);
  });
}

function readFile(filename){
  return JSON.parse(fs.readFileSync(filename, 'utf8'));
}

function Help(message){
  var args = message.content.split(' ');
  if((args[0] === `<@${bot.user.id}>` && args[1] === 'help')
    || (args[0] === '!help')){
    message.channel.send({embed: {
      color: 3447003,
      title: 'I am here to help you',
      description: '*I will follow these commands* \n`` ``',
      fields: readFile('commands.json'),
      footer: {
        icon_url: bot.user.avatarURL,
        text: 'S k y n e t'
      }
    }});
  }
}

var Game = function(message){
  this.startTime = message.createdTimestamp;
  this.attempts = 0;
  this.random = Math.floor(Math.random() * 1000);
  console.log(`New game started at ${message.channel.id}: ` + this.random);
  message.reply('Guess that number!');
}

Game.prototype.guess = function(message){
  this.attempts++;
  if(message.content == this.random){
    updateScore(message, 1, this.attempts);
    var time = message.createdTimestamp - this.startTime;
    message.reply('Gagné ! 🥇🎉🎊\nVous avez gagné en ' + this.attempts
      + `coups et ${time}ms.\n(``!score`` pour le tableau des scores)`);
    return true;
  }
  if(message.content > this.random){
    message.reply('plus petit ⬇️');
  }
  else{
    message.reply('plus grand ⬆️');
  }
  return false;
}

/* Play Guess That Number */
function GuessThatNumber(message){

  var channel = message.channel.id;

  if(message.content === '!g'
    || message.content === '!g-number start'
    || message.content === '!g-number'){
    if(games.has(channel)){
      message.reply('Une partie est déjà en cours.');
    }
    else{
      games.set(channel, new Game(message));
    }
  }

  if(games.has(channel) && !isNaN(message.content)){
    if(games.get(channel).guess(message)){
      games.delete(channel)
    }
  }

  if(message.content === '!g-number stop'){
    if(games.has(channel)){
      games.delete(channel);
      message.reply('Partie stoppée.');
    }
    else{
      message.reply('Pas de partie en cours.');
    }
  }

}

/* Play Ping Pong... Or something */
async function PingPong(message){

  if(message.content === '!ping'){
    const m = await message.channel.send('Pong! ');
    var time =
    m.edit(`Pong! `
      + `Latency is ${m.createdTimestamp - message.createdTimestamp}ms.`
      + `API Latency is ${Math.round(bot.ping)}ms.`
    );
  }

}

/* Call when no commands is given */
function Hello(message){
  if(message.content === `<@${bot.user.id}>`){
    message.channel.send(`Hasta la vista, <@${message.author.id}>! `);
  }
}

function updateScore(message, point, attempts){
  var user_id = message.author.id;
  var data = fs.readFileSync('scores.json', 'utf8');
  var scores = JSON.parse(data);
  if(!(user_id in scores)){
    scores[user_id] = [point, attempts]
  }
  else{
    scores[user_id][0] += point;
    if(scores[user_id][1]>attempts){
      scores[user_id][1] = attempts;
    }
  }
  writeInFile(scores, "scores.json");
}

function Score(message){
  if(message.content === '!score'){
    var table = new Table({
      head: ['Username', 'Record'],
      style: {
        'padding-left': 1, 'padding-right': 1, head: [], border: []
      },
      colWidths: [14, 10]
    });

    var scores = readFile('scores.json');

    var items = Object.keys(scores).map(function(key) {
      return [key, scores[key][1]];
    });
    items.sort(function(first, second) {
      return first[1] - second[1] ;
    });
    items.forEach(function(value){
      table.push([bot.users.get(value[0]).username, value[1]]);
    });

    message.channel.send(`\`\`\`${table.toString()}\`\`\``);
  }
}

function Say(message){
  var args = message.content.split(' ');
  if(args[0] === '!s'){
    if(bot.channels.exists('id', args[1]) ){
      bot.channels.get(arg[1]).send(args.splice(2, args.length));
    }
    else if(message.guild.channels.exists('id', args[1].substring(2, args[1].length-1))){
      bot.channels
        .get(args[1].substring(2, args[1].length-1))
        .send(args.splice(2, args.length).join(' '));
    }
    else{
      message.channel.send(args.splice(1, args.length).join(' '));
    }
  }
}

/* When the bot logs in... */
bot.on('ready', function(){
  bot.user.setActivity('!help', { type: 'WATCHING' }).catch(console.error);
})

/* Read messages */
bot.on('message', async message =>{

  PingPong(message);
  GuessThatNumber(message);
  Hello(message);
  Score(message);
  Say(message);
  Help(message);

});

bot.login(config.token);
